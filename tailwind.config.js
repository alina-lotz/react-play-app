/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    colors: {
      'major-dark': '#7c1034',
      'major-light': '#FAAB99',
      'minor-dark': '#252f9c',
      'minor-light': '#cfe5ff',
    },
    fontFamily: {
      raleway: ['Raleway', 'auto'],
      fredoka: ['Fredoka', 'auto'],
    },
    extend: {},
  },
  plugins: [],
}
