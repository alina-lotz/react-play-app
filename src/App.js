import './App.css';
import './style.css';
import EightBall from './Components/8ball';
import TheStory from './Components/theStory';
import Weather from './Components/weather';
import { useEffect, useState } from 'react';

function App() {
  const [data, setData] = useState([]);

  useEffect(
    () => {
      fetch('https://api.catboys.com/8ball',
      {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
      })
      .then(response => 
        {
          return response.json();
        })
      .then(data => 
        {
          console.log(data);
          setData(data);
        });
    }, []
  )
  
  return (
    <div className='flex'>
      <EightBall />
      <TheStory />
      <Weather />
    </div>
  );
}

export default App;
