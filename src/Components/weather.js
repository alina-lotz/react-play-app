import { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMoon } from '@fortawesome/free-solid-svg-icons'
import { faSnowflake, faSmog, faSun, faCloudSun, faCloudSunRain, faCloud, faCloudBolt, faCloudRain, faCloudShowersHeavy, faCloudShowersWater} from '@fortawesome/free-solid-svg-icons'
import { faTemperatureHigh, faTemperatureLow } from '@fortawesome/free-solid-svg-icons'

function Weather() {
    const [data, setData] = useState([]);
    const [mode, setMode] = useState(false);

    const fetchData = () => {
        fetch('https://api.open-meteo.com/v1/forecast?latitude=44.6166&longitude=33.5254&daily=weathercode,temperature_2m_max,temperature_2m_min&timezone=Europe%2FMoscow',
        {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        })
        .then(response => 
        {
            return response.json();
        })
        .then(data => 
        {
            console.log(data);
            setData(data);
            console.log(data);
        });
    }

    useEffect(
        () => {
            fetchData();
        }, []
    )

    const toggleMode = () => {
        var element = document.getElementById("weather");
        element.classList.toggle("dark");
        setMode(!mode);
    }

    return (
        <div id='weather' className='w-1/3'>
            <div className="flex flex-col pt-40 items-center h-screen w-full 
                bg-minor-light text-minor-dark dark:text-minor-light dark:bg-minor-dark">

                <button onClick={toggleMode}
                    className='absolute top-0 mt-3 mr-3 p-0 w-10 h-10 text-2xl self-end rounded-full
                    bg-minor-dark text-minor-light dark:bg-minor-light dark:text-minor-dark'>
                    <FontAwesomeIcon icon={mode ? faMoon : faSun} className="text-2xl"/>
                </button>
                
                <h2 className='w-full text-center mb-10'> Sevastopol </h2>

                <h1 className="w-full text-center mb-32">Weather</h1>

                {data.daily ?
                    <div className='flex flex-col justify-center h-96'>
                        
                            <ViewWeather 
                                weathercode={data.daily.weathercode[0]} 
                                temperature_2m_max={data.daily.temperature_2m_max[0]} 
                                temperature_2m_min={data.daily.temperature_2m_min[0]} 
                                time={data.daily.time[0].split("-")}
                            />

                            <div className='flex space-x-10 scale-50 -translate-y-11'>
                                <ViewWeather
                                    weathercode={data.daily.weathercode[0]} 
                                    temperature_2m_max={data.daily.temperature_2m_max[0]} 
                                    temperature_2m_min={data.daily.temperature_2m_min[0]} 
                                    time={data.daily.time[0].split("-")}
                                />
                                <ViewWeather 
                                    weathercode={data.daily.weathercode[1]} 
                                    temperature_2m_max={data.daily.temperature_2m_max[1]} 
                                    temperature_2m_min={data.daily.temperature_2m_min[1]} 
                                    time={data.daily.time[1].split("-")}
                                />
                                <ViewWeather 
                                    weathercode={data.daily.weathercode[2]} 
                                    temperature_2m_max={data.daily.temperature_2m_max[2]} 
                                    temperature_2m_min={data.daily.temperature_2m_min[2]} 
                                    time={data.daily.time[2].split("-")}
                                />
                                <ViewWeather
                                    weathercode={data.daily.weathercode[3]} 
                                    temperature_2m_max={data.daily.temperature_2m_max[3]} 
                                    temperature_2m_min={data.daily.temperature_2m_min[3]} 
                                    time={data.daily.time[3].split("-")}
                                />
                                <ViewWeather 
                                    weathercode={data.daily.weathercode[4]} 
                                    temperature_2m_max={data.daily.temperature_2m_max[4]} 
                                    temperature_2m_min={data.daily.temperature_2m_min[4]} 
                                    time={data.daily.time[4].split("-")}
                                />
                                <ViewWeather 
                                    weathercode={data.daily.weathercode[5]} 
                                    temperature_2m_max={data.daily.temperature_2m_max[5]} 
                                    temperature_2m_min={data.daily.temperature_2m_min[5]} 
                                    time={data.daily.time[5].split("-")}
                                />
                                <ViewWeather 
                                    weathercode={data.daily.weathercode[6]} 
                                    temperature_2m_max={data.daily.temperature_2m_max[6]} 
                                    temperature_2m_min={data.daily.temperature_2m_min[6]} 
                                    time={data.daily.time[6].split("-")}
                                />
                            </div>
                    </div>
                : <></>}
                
            </div>
        </div>
    );
}

function ViewWeather(props) {
    const Smog = new Set([45, 48]);
    const Sun = new Set([0]);
    const CloudSun = new Set([1, 2]);
    const CloudSunRain = new Set([]);
    const Cloud = new Set([3]);
    const CloudBolt = new Set([]);
    const CloudRain = new Set([51, 53, 55, 56, 57]);
    const CloudShowersHeavy = new Set([61, 63, 65, 66, 67]);
    const CloudShowersWater = new Set([80, 81, 82, 95]);
    const Snowflake = new Set([71, 73, 75, 77, 85, 86]);

    return (
        <div className='flex flex-col items-center space-y-3'>

            <div className='flex flex-col items-center'>
                <p className='font-fredoka font-normal'>{ props.time[2] + "." + props.time[1] } </p>
                <p className='font-fredoka'>{ props.time[0] } </p>
            </div>

            <FontAwesomeIcon icon={ (Smog.has(props.weathercode)) ? faSmog 
                : (Sun.has(props.weathercode)) ? faSun
                : (CloudSun.has(props.weathercode)) ? faCloudSun
                : (Cloud.has(props.weathercode)) ? faCloud
                : (CloudRain.has(props.weathercode)) ? faCloudRain
                : (CloudShowersHeavy.has(props.weathercode)) ? faCloudShowersHeavy
                : (CloudShowersWater.has(props.weathercode)) ? faCloudShowersWater
                : (Snowflake.has(props.weathercode)) ? faSnowflake 
                : faCloudSun
            } className='text-8xl'/>

            <div className='flex flex-col items-center scale-75'>
                <div className='flex space-x-3'>
                    <p className='font-fredoka'>{props.temperature_2m_max}</p>
                    <FontAwesomeIcon icon={faTemperatureHigh} className='text-3xl' />
                </div>
                <div className='flex space-x-3'>
                    <p className='font-fredoka'>{props.temperature_2m_min}</p>
                    <FontAwesomeIcon icon={faTemperatureLow} className='text-3xl' />
                </div>
            </div>
            
        </div>
    );
}

export default Weather;