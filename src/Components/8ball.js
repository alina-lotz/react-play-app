import { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRotate, faMoon, faSun } from '@fortawesome/free-solid-svg-icons'

function EightBall() {
    const [data, setData] = useState([]);
    const [mode, setMode] = useState(false);

    const fetchData = () => {
        fetch('https://api.catboys.com/8ball',
        {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        })
        .then(response => 
        {
            return response.json();
        })
        .then(data => 
        {
            console.log(data);
            setData(data);
        });
    }

    useEffect(
        () => {
            fetchData();
        }, []
    )

    const toggleMode = () => {
        var element = document.getElementById("8ball");
        element.classList.toggle("dark");
        setMode(!mode);
    }

    return (
        <div id='8ball' className='w-1/3'>
            <div className="flex flex-col pt-40 items-center h-screen w-full 
                bg-major-dark text-minor-light dark:text-minor-dark dark:bg-major-light">

                <button onClick={toggleMode}
                    className='absolute top-0 right-2/3 mt-3 mr-3 p-0 w-10 h-10 text-2xl self-end rounded-full
                    bg-minor-light text-major-dark dark:bg-minor-dark dark:text-major-light'>
                    <FontAwesomeIcon icon={mode ? faSun : faMoon} className="text-2xl"/>
                </button>
                
                <h2 className='w-full text-center mb-10'> {data.response} </h2>

                <h1 className="w-full text-center mb-20">8ball</h1>
                
                <div className='flex flex-col justify-center h-96'>
                    <div className='ball bg-minor-light dark:bg-minor-dark rounded-full overflow-hidden shadow-xl shadow-[#00000040] '>
                        <div className='h-48 w-48 overflow-hidden rounded-full flex items-center border-2
                            border-minor-light dark:border-minor-dark'>
                            <img className="h-52 w-52 rounded-full object-cover" src={data.url}></img>
                        </div>

                        <button className='text-major-dark dark:text-major-light bg-none w-full text-center mt-7' 
                            onClick={fetchData}>
                            <FontAwesomeIcon icon={faRotate} className="text-6xl"/>
                        </button>
                    </div>
                </div>
                
            </div>
        </div>
    );
}

export default EightBall;