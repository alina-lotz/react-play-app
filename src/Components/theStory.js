import { useEffect, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRotate, faMoon, faSun } from '@fortawesome/free-solid-svg-icons'

import { faUserAstronaut, faDragon, faUserTie, faSnowman, faChildren, faPersonMilitaryRifle} from '@fortawesome/free-solid-svg-icons' //hero
import { faShuttleSpace, faCat, faBroom, faCompass, faWandSparkles, faCrow} from '@fortawesome/free-solid-svg-icons' //friend   
import { faShrimp, faHandHoldingMedical, faBrain, faSackDollar, faHeart, faHatWizard} from '@fortawesome/free-solid-svg-icons' //goal
import { faMeteor, faSpider, faMosquito, faVolcano, faGhost, faSkull} from '@fortawesome/free-solid-svg-icons' //obstacle
import { faFaceMehBlank, faFaceGrimace, faFaceAngry, faFaceGrin, faFaceDizzy, faFaceGrinStars } from '@fortawesome/free-solid-svg-icons' //theEnd

function TheStory() {
    const [data, setData] = useState([]);
    const [mode, setMode] = useState(false);
    const [iconIndex, setIconIndex] = useState(0);
    const [isIcons, setIsIcons] = useState(0);
    const [showIcons, setShowIcons] = useState(false);
    const [diceResult, setDiceResult] = useState([]);

    const fetchData = () => {
        fetch('https://api.catboys.com/dice',
        {
        method: 'GET', // *GET, POST, PUT, DELETE, etc.
        })
        .then(response => 
        {
            return response.json();
        })
        .then(data => 
        {
            console.log(data);
            if(data.response == 0) 
                fetchData();
            else 
            {
                setData(data);
                
                if(!showIcons && diceResult.length < 5) {
                    diceResult.push(data.response);
                    setDiceResult(diceResult);
                }

                if (iconIndex < 4 && showIcons) setIconIndex(iconIndex + 1);
                setShowIcons(!showIcons);
                if (iconIndex == 4 && isIcons < 3) setIsIcons(isIcons + 1);
            }
            
            if(data.response == 1) data.url="https://cdn.catboys.com/dice/1.png";
        });
    }
    
    const toggleMode = () => {
        let element = document.getElementById("TheStory");
        element.classList.toggle("dark");
        setMode(!mode);
    }

    const toggleBouncing = () => {
        let element = document.getElementById("diceImg");
        element.classList.toggle("animate-bounce");
    }

    return (
        <div id='TheStory' className='w-1/3'>
            <div className="flex flex-col pt-40 items-center h-screen w-full 
                bg-major-light text-major-dark dark:text-major-light dark:bg-major-dark">

                <button onClick={toggleMode}
                    className='absolute top-0 right-1/3 mt-3 mr-3 p-0 w-10 h-10 text-2xl self-end rounded-full
                    bg-major-dark text-major-light dark:bg-major-light dark:text-major-dark'>
                    <FontAwesomeIcon icon={mode ? faMoon : faSun} className="text-2xl"/>
                </button>

                <h2 className='w-full text-center mb-10'> Dice your Story </h2>

                <h1 className="w-full text-center mb-20">Story</h1>
                
                <div className='flex flex-col justify-center items-center h-96'>

                    <button className=''
                        onClick={()=>{
                            setData([]);
                            setMode(false);
                            setIconIndex(0);
                            setIsIcons(0);
                            setShowIcons(false);
                            setDiceResult([]);
                        }}>
                        <FontAwesomeIcon icon={(isIcons < 2) ? "" : faRotate } className="text-2xl"/>
                    </button>

                    <DiceStory 
                        num = { data.response } 
                        counter = {iconIndex} 
                        is = {isIcons} 
                        show = {showIcons} 
                        result = {diceResult}
                    />

                    <button className='p-0 h-40 flex flex-col justify-end items-center mt-5' 
                        onClick={()=>{
                            fetchData();
                        }}
                        onMouseEnter={toggleBouncing} onMouseLeave={toggleBouncing}>
                        <img className="dark:invert outline outline-4 outline-[#000000] outline-offset-[-3px] 
                            h-32 w-32 rounded-3xl bg-white shadow-xl shadow-[#00000040] dark:shadow-[#ffffff40]" 
                            id='diceImg' src={data.url ? data.url : "https://cdn.catboys.com/dice/3.png"}></img>
                    </button>

                </div>
                
            </div>
        </div>
    );
}

function DiceStory(props) {

    //Dialogs
    const dialog = [
        "Who would you like to be in this story?",
        "Choose your best friend :)",
        "What is your goal?",
        "But what will become an obstacle?!",
        "And the end will be...",
    ];

    //1 - Hero content
    const heroIcon = [faUserAstronaut, faDragon, faUserTie, faSnowman, faChildren, faPersonMilitaryRifle];
    const heroText = [
        "You're a brave and courageous Astronaut traveling to planets and galaxies, but no matter how brave you are, you needed a best friend.", //
        "DragonYour wings cut through the air, you feel like a flame is burning inside you, because you are a real dragon. Now you are looking down on your friend.", 
        "Your boring life as an office worker has fed up with you, so you decided to test yourself, but for this you need a partner!",
        "Frost is your element, unfortunately no one takes you seriously, so you decided to prove to everyone that you are capable of a lot. Where is your friend?",
        "Children cannot sit still in any way and they are always drawn to adventures, or vice versa, the adventure itself comes to them. Who will they meet on their way?",
        "After a hard battle, you realize that the goal has not yet been achieved, you feel lonely after all the losses. But you are not alone.",
    ];
    
    //2 - Friend content
    const friendIcon = [faShuttleSpace, faCat, faBroom, faCompass, faWandSparkles, faCrow];
    const friendText = [
        "A great and the fastest spaceship in the entire galaxy landed nearby, which will allow you to visit all corners of this world.Spaceship", //a  Spaceship 
        "CatA black cat came up to you, he said Meow and you decided that now he is with you forever.", 
        "Suddenly a magic broom flew right up to your feet to soar with you into the sky.",
        "Not far away you noticed a discarded compass that points not to the North.",
        "You bought a magic wand at Ollivander's store.",
        "You helped the wise raven get out of the net, now he will help you.",
    ];
    
    //3 - Goal content
    const goalIcon = [faShrimp, faHandHoldingMedical, faBrain, faSackDollar, faHeart, faHatWizard];
    const goalText = [
        "Your goal is the tastiest and pinkest shrimp in the entire universe.", 
        "Your goal is to save people from all the troubles on the planet.", 
        "Your goal is to gain new knowledge and wisdom along the way.",
        "Your goal has always been wealth.",
        "You want to find love, you understand that it is not so easy to find it.",
        "The wizard hat! Here's what you couldn't find in Ollivander's store.",
    ];

    //4 - Obstacle content
    const obstacleIcon = [faMeteor, faSpider, faMosquito, faVolcano, faGhost, faSkull];
    const obstacleText = [
        "Suddenly you realize that a bright meteor flying directly above you, if it falls, it will leave nothing behind",
        "Be careful, you almost stepped on a poisonous spider, there will be many of them on the way.", 
        "A bunch of mosquitoes attack at night, you need to make it before sunset.",
        "Suddenly, the earth goes out from under your feet, you realize that a volcano is erupting next to you.",
        "At night you notice that you are not alone, someone is watching you and it does not give you peace.",
        "Death is on your heels, you resist as much as you can.",
    ];

    //5 - TheEnd content
    const theEndIcon = [faFaceMehBlank, faFaceGrimace, faFaceAngry, faFaceGrin, faFaceDizzy, faFaceGrinStars];
    const theEndText = [
        "You become speechless because you can't figure out how to talk about what you saw that day. You're not normal anymore but your friend can help you.",
        "For some unknown reason, you feel very cold, but this does not stop you. Your friend warms your soul anyway.",
        "You understand that for this reason you will not be able to achieve your goal and the desire to fight disappears.",
        "You find it! Happiness overwhelms you. You were able to overcome all the challenges and even the prize does not seem as important as what you experienced with a friend.",
        "She found you and you didn't find her, death won't come back.",
        "Do you find the treasure completely by accident, or not? Have you finally found what you were looking for? Where is you friend?",
    ];

    //All Content
    const currIcons = [heroIcon, friendIcon, goalIcon, obstacleIcon, theEndIcon];
    const currText = [heroText, friendText, goalText, obstacleText, theEndText]

    const listIcons = currIcons[props.counter].map((icon, index) =>
        <FontAwesomeIcon icon={icon} className={
            (index == props.num - 1 && props.show) ? "text-minor-dark dark:text-minor-light" : ""
        }/>
    );

    let storyResult = "";

    const listText = props.result.map((elem, index) =>
        {storyResult += currText[index][elem-1] + " ";}
    );

    return(
        <div className='flex flex-col items-center space-y-10'>
            <p className='h-fit flex flex-wrap justify-center space-x-3 px-5 text-center'>
                { ((props.is < 2)) ?  dialog[props.counter] : listText }
                <p className='h-fit font-fredoka text-xl px-5 text-center leading-6'>
                    {((props.is < 2)) ?  "" : storyResult}
                </p>
            </p>
            
            <h2 className='flex space-x-5'>
                { ((props.is < 2)) ? listIcons : "" }
            </h2>
        </div>
    );
}

export default TheStory;